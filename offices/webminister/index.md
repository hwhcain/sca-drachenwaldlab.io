---
title: Office of the Web Minister
excerpt: Who keeps this online circus running?

---

## Kingdom web artificers

These folks volunteer to work on the kingdom website and other IT affairs

* Sela de la Rosa
* Genevieve la flechiere
* Marcella di Cavellino
* Menja (Mallymkun Galpin)
* Maria Harsick

## Google Workspace
* [Dragon's tale articles](https://members.sca.org/apps/newsletters/Drachenwald/Drachenwald%20DRAGONS%20TALE%202204.pdf)
* [FAQ](https://docs.google.com/document/d/1kQaMzJhJLQJAIPQKwkxPpGVFNGIl6WQId2WqTsaYc3g/edit)
* [Drachenwald Google Workspace Policy](https://docs.google.com/document/d/1wgzxoUSlMYCvkeR2s7vwwBx70FPB3zfx1eTFnLLil0w/edit?usp=sharing)

## Reporting for principality and local webministers

Mostly, Yda would like to hear from web ministers and would like to know if there's anything you're running into that kingdom web artificers can help with. Beyond that she appreciates getting an update on major things planned or that happened.  

## Kingdom site issues

Send a report to the Gitlab issues tracker: <a href="https://gitlab.com/sca-drachenwald/sca-drachenwald.gitlab.io/-/issues/new"> 


## Updating information on the website
* [Update officer information](https://forms.gle/Xm7bCu7nkq5uMU5Z6)
* [Adding a chartered group to the website]({{ site.baseurl}}{% link offices/webminister/content-policy.md %}) 

## Tools for group webministers

* [Live contact information for your group]({{ site.baseurl }}{% link offices/webminister/officer-code-creator.html %})

{% include officer-contacts.html %}
